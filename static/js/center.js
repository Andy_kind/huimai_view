$(document).ready(function() {

//    var len=$(".silderUl li").length;
//    for(var i=1;i<=len;i++){
//        if(i%4==0){
//            $(".silderUl li").eq(i-1).css("margin-right","0");
//        }
//    }
    $(".settle-table tr:last-child td").css("border-bottom","0");
    $(".pay-month .pay-month-ul:last-child").css("border-bottom","0");
    $(function () {
        $(".menu_div-two-bb:nth-child(2)").css({"margin-top": "-15px"});
        $(".menu-divBottom a:nth-child(5) img ,.menu-divBottom a:nth-child(10) img").css({
            "border-right": "0px",
            "width": "90px"
        });
        $(".menu_div .tit-li").hover(function () {
            var cla=$(this).find("b").attr("class");
            $(this).find("b").addClass(cla+"_2");
        }, function () {
            var cla=$(this).find("b").attr("class");
            var subcla = cla.substring(2, cla.length);
            $(this).find("b").removeClass(subcla);
        });
    });

    $(".feinei_tit").hover(function () {
        $(this).parents(".feilei").find(".menu_div").css("display","block");
    }, function () {
        $(this).parents(".feilei").find(".menu_div").hover(function () {
        }, function () {
            $(this).parents(".feilei").find(".menu_div").css("display","none");
        });
    });

    for(var i=0;i<$(".user-item").length;i++){
        if($(".user-item").eq(i).find("dt").attr("attr")==2){
            $(".user-item").eq(i).find("dt").next(".disp-dnk").css("display","block");
        }
        if(i==$(".user-item").length-1){
            $(".user-item").eq(i).find("dt").css("margin-bottom","0");
        }
    }
    $(".user-item dt").click(function () {
        var attr=$(this).attr("attr");
        if(attr==1){
            $(this).attr("attr",2);
            $(this).next(".disp-dnk").css("display","block");
        }else{
            $(this).attr("attr",1);
            $(this).next(".disp-dnk").css("display","none");
        }
    });
    /*积分中心*/
    for(var i=1;i<=$(".jf-spList li").length;i++) {
        if(i%4==0){
            $(".jf-spList li").eq(i-1).css("margin-right","0");
        }
    }
    /*商品评论*/
    $(function(){
        var str2 = "亲，写点什么吧，您的意见对其它网友有很大帮助";
        var vale = $(".textea").val();
        $(".textea").focus(function() {
            if (this.value == str2){
                this.value = '';
            }
        });
        $(".textea").blur(function() {
            if (this.value == ''){
                this.value = vale;
            }
        });
    });
});


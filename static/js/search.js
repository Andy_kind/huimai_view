function supports_history_api(){
	return !!(window.history && history.pushState);
}
function getCookie(name){
    var start = document.cookie.indexOf(name + "=");
    var len = start + name.length + 1;
    if ((!start) && (name != document.cookie.substring(0, name.length))) {
    return null;
    }
    if (start == -1) return null;
    var end = document.cookie.indexOf(';', len);
    if (end == -1) end = document.cookie.length;
    return unescape(document.cookie.substring(len, end));
}
function bindFilters(){
    if(window.location.href.indexOf('?') !== -1){
        var queryString = location.href.split('?')[1];
        var params = queryString.split('&');
        for(var i = 0; i < params.length; i++){
            if(params[i].indexOf('=') !== -1){
                var param = params[i].split('=');
                switch(param[0]){
                    case "keywords":
                        $('.sousuo_text').val(decodeURIComponent(param[1]));
                        break;
                    case "r_type":
                        var r_types = decodeURIComponent(param[1]).split('-');
                        for(var ri = 0; ri < r_types.length; ri++){
                            $('.li1 label').each(function(){
                                if($(this).find('span').html() == r_types[ri]){
                                    $(this).find('input').prop('checked', true);
                                }
                            });
                        }
                        break;
                    case "discount":
                        var discounts = decodeURIComponent(param[1]).split('-');
                        for(var di = 0; di < discounts.length; di++){
                            $('.li2 label').each(function(){
                                if($(this).find('span').html() == discounts[di]){
                                    $(this).find('input').prop('checked', true);
                                }
                            });
                        }
                        break;
                    case "sites":
                        var sites = decodeURIComponent(param[1]).split('-');
                        for(var si = 0; si < sites.length; si++){
                            $('.li3 label').each(function(){
                                if($(this).find('span').html() == sites[si]){
                                    $(this).find('input').prop('checked', true);
                                }
                            });
                        }
                        break;
                    case "local":
                        var locals = decodeURIComponent(param[1]).split('-');
                        for(var li = 0; li < locals.length; li++){
                            $('.li4 label').each(function(){
                                if($(this).find('span').html() == locals[li]){
                                    $(this).find('input').prop('checked', true);
                                }
                            });
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
function load(isPopState=false){
    var keyword = $('.sousuo_text').val();
    var r_types = [];
    var discounts = [];
    var sites = [];
    var locals = [];
    $('.li1 label').each(function(){
        var checkbox = $(this).find('input');
        if(checkbox.prop('checked')){
            r_types.push($(this).find('span').html());
        }
    });
    $('.li2 label').each(function(){
        var checkbox = $(this).find('input');
        if(checkbox.prop('checked')){
            discounts.push($(this).find('span').html());
        }
    });
    $('.li3 label').each(function(){
        var checkbox = $(this).find('input');
        if(checkbox.prop('checked')){
            sites.push($(this).find('span').html());
        }
    });
    $('.li4 label').each(function(){
        var checkbox = $(this).find('input');
        if(checkbox.prop('checked')){
            locals.push($(this).find('span').html());
        }
    });
    //console.log(keyword, r_types, discounts, sites, locals);
    var url = '/data/query';
    var query = [];
    if(keyword != '') query.push('keywords=' + encodeURIComponent(keyword));
    if(r_types.length > 0) query.push('r_type=' + encodeURIComponent(r_types.join('-')));
    if(discounts.length > 0) query.push('discount=' + encodeURIComponent(discounts.join('-')));
    //if(sites.length > 0) query.push('site=' + encodeURIComponent(sites.join('-')));//文档中没有此选项
    if(locals.length > 0) query.push('local=' + encodeURIComponent(locals.join('-')));
    if(query.length > 0) url += '?' + query.join('&');
    console.log(url);
    if(supports_history_api()){
        if(!isPopState){
            window.history.pushState({title:"搜索",url:url}, '', url);
        }
        $.post(url,{_xsrf:getCookie('xsrf')},function(data){
            $('div.item').html(data);
        });
    } else {
        window.location.href = url;
    }
    
}
//load();
$(document).ready(function(){
    bindFilters();
    if(supports_history_api()){
        window.onpopstate = function(event){
            $('.left-pfxed input').prop('checked', false);
            $('.sousuo_text').val('');
            bindFilters();
            console.log(location.href);
            load(true);
        }
    }
    $('.left-pfxed input').change(function(){
        load();
    });
    $('.sousuo_btn').click(function(){
        load();
    });
    setInterval(function(){load(false)}, 5000);
});
$.fn.silder_banner = function (prev_btn, next_btn, number, selected) {
    return this.each(function () {
        var container = $(this).find("div").css("overflow", "hidden");
        var ul = container.find("ul").width(999999);
        var li = ul.find("li");//获取全部li
        var li_first = li.filter("li:first");//获取第一个li
        var li_width = li_first.width();//获取单个li的宽度
        var line_number = Math.ceil(container.innerWidth() / li_width);//一行可以放多少个li
        var pages = Math.ceil(li.length / line_number);
        var currentPage = 1;
        li.filter("li:first").before(li.slice(-line_number).clone().addClass("cloned"));
        li.filter("li:last").after(li.slice(0, line_number).clone().addClass("cloned"));
        li = ul.find("li");
        container.scrollLeft(li_width * line_number);
        function goto_Page(page) {
            var dir = page < currentPage ? -1 : 1;/*1代表下一个,-1代表上一个*/
            var n = Math.abs(currentPage - page);
            var left = li_width * dir * line_number * n;
            container.filter(":not(:animated)").animate({scrollLeft: "+=" + left}, 500, function () {
                if (page > pages) {
                    container.scrollLeft(li_width * line_number);
                    page = 1;
                } else if (page == 0) {
                    page = pages;
                    container.scrollLeft(li_width * line_number * pages);
                }
//                alert(page);
                if(page==0){
                    $(number).eq(page).addClass(selected).siblings().removeClass(selected);
                }else{
                    $(number).eq(page-1).addClass(selected).siblings().removeClass(selected);
                }
                currentPage = page;
            });
        }
        $(prev_btn).click(function () {
            goto_Page(currentPage - 1);
            return false;
        });
        $(next_btn).click(function () {
            goto_Page(currentPage + 1);
            return  false;
        });
        $(number).click(function () {
            goto_Page($(this).index()+1);
            return false;
        });
        $(this).bind("next", function () {
            goto_Page(currentPage + 1);
        });
    });
};

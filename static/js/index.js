$(document).ready(function() {

    $(function () {
        $(".menu_div-two-bb:nth-child(2)").css({"margin-top": "-15px"});
        $(".menu-divBottom a:nth-child(5) img ,.menu-divBottom a:nth-child(10) img").css({
            "border-right": "0px",
            "width": "90px"
        });
        $(".menu_div .tit-li").hover(function () {
            var cla=$(this).find("b").attr("class");
            $(this).find("b").addClass(cla+"_2");
        }, function () {
            var cla=$(this).find("b").attr("class");
            var subcla = cla.substring(2, cla.length);
            $(this).find("b").removeClass(subcla);
        });
    });



    /*banner*/
    var index=0;
    var banner_time=0;
    var img=$('.silder_ul li');
    var img_length=$(".silder_ul li").length;

    $('.silder-btn li,.silder_ul li').hover(function () {/*鼠标移动到数字按钮上*/
        clearInterval(banner_time);
    }, function () {
        banner_time = setInterval(banner,5000)
    });
    $('.silder-btn li').click(function () {/*鼠标点击数字按钮*/
        index=$(this).index();
        img.eq(index).stop().fadeIn(300).siblings().stop().fadeOut(300);
        $(this).addClass("current").siblings().removeClass("current");
    });
    $(document).ready(function(){
        banner_time = setInterval(banner,5000)
    });
    function banner(){
        index++;
        if(index>img_length-1){
            index=0;
        }
        img.eq(index).fadeIn(500).siblings().fadeOut(500);
        img.removeClass('silder-index');
        $('.silder-btn li').eq(index).addClass('current').siblings().removeClass("current");
    }
    $(function () {
        $(".new-sp-ul li:first-child").css({"margin-left": 0, "width": "238px"});
        $(".product_tab_ul  li:last-child a").css("border-right", " 1px solid #cccccc");
        $(".phb-container.phb-dl:last-child").css("border-bottom",0);
        $(".end-ul li:last-child").css('border-right','0');
        $(".product_tab_ul li a").hover(function () {
            $(this).parents(".product-list").find(".product_tab_ul li a").removeClass("current");
            $(this).addClass("current")
        });

        /*广告位滚动*/
        $(document).ready(function () {
            var tf = true;
            $(".silder-adver1").hover(function () {
                tf=false;
                return tf;
            }, function () {
                tf=true;
                return tf;
            });
            $(".silder-adver1").silder_banner(".left_p1", ".right_p1",".uls1 li" , "selected");

            setInterval(function () {
                if(tf==true){
                    $(".silder-adver1").trigger("next");
                }
            },4000)
        });
        $(document).ready(function () {
            var tf = true;
            $(".silder-adver2").hover(function () {
                tf=false;
                return tf;
            }, function () {
                tf=true;
                return tf;
            });
            $(".silder-adver2").silder_banner(".left_p2", ".right_p2",".uls2 li" , "selected");

            setInterval(function () {
                if(tf==true){
                    $(".silder-adver2").trigger("next");
                }
            },5500)
        });
        $(document).ready(function () {
            var tf = true;
            $(".silder-adver3").hover(function () {
                tf=false;
                return tf;
            }, function () {
                tf=true;
                return tf;
            });
            $(".silder-adver3").silder_banner(".left_p3", ".right_p3",".uls3 li" , "selected");

            setInterval(function () {
                if(tf==true){
                    $(".silder-adver3").trigger("next");
                }
            },4500)
        });
        /*排行榜*/
        $(".phb-container .phb-dl").hover(function () {
            $(this).parents(".phb-container").find($(".phb-container .phb-dl")).find("i").removeClass("phb-number-selected");
            $(this).find("i").addClass("phb-number-selected")
        });
        /*漂浮*/
        $(window).scroll(function () {
            var scroll_top=$(document).scrollTop();
            if(scroll_top>$(".new-sp-ul").offset().top){
                $(".pfxed").fadeIn(500);
            }else{
                $(".pfxed").fadeOut(100);
            }
        });
        /*导航条*/
        var nav_top=$(".nav").offset().top;
        var wet_top=$(".wet-list-div").offset().top;
        window.onscroll = function () {
            var scroll_height = document.documentElement.scrollTop || document.body.scrollTop;
            if (nav_top >= scroll_height) {/*还原*/
                $(".nav").css({"position":"","margin-top":"15px"});
                $(".menu_div").css("display","block");
            }
            if (nav_top < scroll_height) {
                $(".nav").css({"position":"fixed","top":0,"margin-top":"0px"});
                $(".menu_div").css("display","none");
                $(".feinei_tit").hover(function () {
                    $(this).parents(".feilei").find(".menu_div").css("display","block");
                }, function () {
                    $(this).parents(".feilei").find(".menu_div").hover(function () {
                    }, function () {
                        if ($(".nav").css("position") == "fixed"){
                            $(this).parents(".feilei").find(".menu_div").css("display", "none");
                        }
                    });
                })
            }
            if(wet_top-200<scroll_height){
                $(".disc").addClass("disc-opac1");
            }
        };
        $('a[href*=#],area[href*=#]').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var $target = $(this.hash);
                $target = $target.length && $target || $('[name=' + this.hash.slice(1) + ']');
                if ($target.length) {
                    var targetOffset = $target.offset().top;
                    $("html body").animate({scrollTop: targetOffset},500);
                    return false;
                }
            }
        });
    });
});


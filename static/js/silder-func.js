$.fn.silder_PN = function (silder_container,silder_container_li,prev_btn,next_btn,number,img_class) {
    var index=0;
    var left=$(silder_container).css("left");
    var width=$(silder_container_li).width();
    var len=$(silder_container_li).length;

    $(prev_btn).click(function () {
        if(index<=0){
            index=0;
            return;
        }
        if(index%4==0){
            left=parseInt(left)+(number*(width+10));
            $(silder_container_li).css("left",left);
        }
        index--;
        qh(index);
    }),
    $(next_btn).click(function () {
        index++;
        if(index>=len){
            index=len;
            return;
        }
        if(index%number==0){
            left=parseInt(left)-(number*(width+10));
            $(silder_container_li).css("left",left);
        }
        qh(index);
    }),
    $(silder_container_li).click(function () {
        index=$(this).index();
        qh(index);
    });
    var qh= function (index) {
        $(silder_container_li).find("img").removeClass(img_class);
        $(silder_container_li).eq(index).find("img").addClass(img_class);
        $('.booth img').attr('src',$(silder_container_li).eq(index).find("img").attr('src'));
    };

//	$('.f-left-ul li').hover(function(){
//		index_ul=$(this).index();
//		$('.f-left-ul li').find("img").removeClass('current');
//		$('.f-left-ul li').eq(index_ul).find("img").addClass('current');
//		$('.spda_img').attr('src',$('.f-left-ul li').eq(index_ul).find("img").attr('src'));
//	});
};
